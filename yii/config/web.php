<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'hu',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'N7jOcGwpo28mGYm3iiQnoW77dF8J5cR4',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',

            'viewPath' => '@app/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
            /// mailer config a swiftmailer config szerint
            'transport' => [
                            'class' => 'Swift_SmtpTransport',
                            //'encryption' => 'tls',
                            'host' => 'mail.tolna.net', //   smtp.gmail.com
                            'port' => '25', // 25 tolna.net 587 gmail
                            'username' => 'szabika@tolna.net', // szabika@tolna.net
                            'password' => 'Fibonacci',
                            // kell ez ide?
                            'streamOptions' => [
                                'ssl' => ['allow_self_signed' => true, 'confirm_peer' => false, 'verify_peer' => false, 'verify_peer_name' => false],
                            ],
             ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

                // kizarom a listview-bol az insert / update / delete gombokat

                'cikkek/<action:(index|update|create|delete)>' => 'cikkek/<action>',
                'cikkek/<link>' => 'cikkek/view'
            ],
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}
/**
 *
 * A Model / crud generátor (gii) miatt teszt funkció
 */
if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii']=[
        'class' =>  'yii\gii\Module',
        'allowedIPs' => ['*'],
    ];
}

return $config;
