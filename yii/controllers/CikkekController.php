<?php

namespace app\controllers;

use Yii;
use app\models\Cikkek;
use app\models\CikkekSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CikkekController implements the CRUD actions for Cikkek model.
 */
class CikkekController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cikkek models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CikkekSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cikkek model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($link) // id
    {
        return $this->render('view', [
            'model' => $this->findModel($link), // id
        ]);
    }

    /**
     * Creates a new Cikkek model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cikkek();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'link' => $model->link]);           // ['view', 'id' => $model->id]
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Cikkek model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($link)     // id
    {
        $model = $this->findModel($link);   // id

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'link' => $model->link]);               // ['view', 'id' => $model->id]
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cikkek model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($link) // id
    {
        $this->findModel($link)->delete();  //id

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cikkek model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cikkek the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($link)// id
    {
        if (($model = Cikkek::findOne(['link' => $link])) !== null) {  // ($id)
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
