<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cikkek */

$this->title = 'Oldal létrehozása';
$this->params['breadcrumbs'][] = ['label' => 'Cikkek', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
