<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cikkek */

$this->title = $model->cim; // id
$this->params['breadcrumbs'][] = ['label' => 'Cikkek', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p class="text-muted">
        <small> Felvitte (id): <?php echo $model->created_by; ?></small>
    </p>

    <?php if (!Yii::$app->user->isGuest) : ?>
        <p>
            <?= Html::a('Update', ['update', 'link' => $model->link], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'link' => $model->link], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Biztos, hogy törlöd ezt az elemet?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif; ?>

    <div>
        <?php echo $model->getEncodedBody(); ?>
    </div>
    <hr/>
    <p><?php echo "<strong>Keywords: </strong>" . \yii\helpers\Html::encode($model->meta_keys) ?></p>
    <hr/>

</div>
