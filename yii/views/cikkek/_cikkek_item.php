<?php
/**
 * Created by PhpStorm.
 * User: szaboszabolcs
 * Date: 2021. 03. 18.
 * Time: 20:54
 */
/** @var $model \app\models\Cikkek */
?>

<div>
    <a href="<?php echo \yii\helpers\Url::to(['/cikkek/view', 'link' => $model->link]); ?>" >      <!-- ['/article/view', 'id' => $model->id] -->
        <h3> <?php echo \yii\helpers\Html::encode($model->cim) ?> </h3>
    </a>
    <div>
<!--        <?php //echo \yii\helpers\HtmlPurifier::process($model->text) ?> -->
        <?php echo \yii\helpers\StringHelper::truncateWords($model->getEncodedBody(), 80) ?>
        <hr />
<!--        <p>--><?php //echo \yii\helpers\Html::encode(implode(', ', $model->meta_keys)) ?><!--</p>-->
        <p><?php echo "<strong>Keywords: </strong>" . \yii\helpers\Html::encode($model->meta_keys) ?></p>
        <hr />
    </div>
</div>