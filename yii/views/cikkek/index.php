<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CikkekSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var $model \app\models\Cikkek */

$this->title = 'Cikkek';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php if (!Yii::$app->user->isGuest) : ?>
        <p>
            <?= Html::a('Új oldal létrehozása', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>





    <?php if (!Yii::$app->user->isGuest) : ?>
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,

            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'cim',
                'link',
                'meta_txt',
                'meta_keys',
                //'text:ntext',
                //'created_by',



                ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Műveletek',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'template' => '{view} {update} {delete}',

                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Megnéz'),
                            ]);
                        },

                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Szerkeszt'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [

                                'data' => [
                                    'confirm' => 'Biztos, hogy törlöd ezt az elemet?',
                                    'method' => 'post',
                                ],
                            ]);
                        }

                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'view') {
//                            $url ='index.php?r=client-login/lead-view&id='.$model->id;
                            $url ='/yii/web/cikkek/'.$model->link;
                            return $url;
                        }

                        if ($action === 'update') {
                            $url ='/yii/web/cikkek/update?link='.$model->link;
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url ='/yii/web/cikkek/delete?link='.$model->link;
                            return $url;
                        }


                    }
                ],

            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    <?php else: ?>

    <?= \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_cikkek_item'
    ]);
    ?>
    <?php endif; ?>



</div>
