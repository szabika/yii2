<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;


/* @var $this yii\web\View */
/* @var $model app\models\Cikkek */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cim')->textInput(['maxlength' => true])->label('Cím') ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true])->label('Link (automata html-barát)') ?>

    <?= $form->field($model, 'meta_txt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_keys')->textInput(['maxlength' => true]) ?>

<!--    <?//= $form->field($model, 'text')->textarea(['rows' => 6]) ?> -->
    <?= $form->field($model, 'text')->widget(CKEditor::className(),
        [
            'kcfinder' => true,
            'kcfOptions' => [
//                'uploadURL' => '@web/upload',
//                'uploadDir' => '@webroot/upload',
                'uploadURL' => Yii::getAlias('@web').'/upload',
                'uploadDir' => Yii::getAlias('@webroot').'/upload',
                ],
//            'options' => ['rows' => 30],
            'clientOptions' => ['height' => 500],
            'preset' => 'basic'
            ])
    ?>

    <?php $usermost = Yii::$app->user->identity->id ?>    <!-- Yii::$app->user->identity->username -->

    <?= $form->field($model, 'created_by')->textInput(['value' => $usermost])->label('Created by (automata user id)') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
