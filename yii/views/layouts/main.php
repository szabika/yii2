<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- -->

    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<!-- ********************************************************************************************************************************************** -->

<body>
<?php $this->beginBody() ?>


<?php

?>
<?php $url = Yii::$app->basePath.'/views/site/head.php'; include $url; ?>



<!-- ********************************************************************************************************************************************** -->

<div class="wrap" style="margin-top:-50px;">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Kezdőlap', 'url' => ['/site/index']],
            ['label' => 'Rólam', 'url' => ['/site/about']],

            ['label' => 'Kapcsolat', 'url' => ['/site/contact']],


            Yii::$app->user->isGuest ? ( ['label' => 'Cikkek', 'url' => ['/cikkek']] ) : (['label' => 'Admin panel', 'url' => ['/cikkek']]),   // csak belepve

            Yii::$app->user->isGuest ? (
                ['label' => 'Belépés', 'url' => ['/site/login']]
            ) : (

                '<li>'

                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Kilépés (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>


    <div class="container" style=" width:100%; background-color:#f5f5f5; height:10px; margin-top:50px; margin-bottom:0; padding-bottom:0"> &nbsp; </div>

    <div class="container" style="margin-top:-120px">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>


        <!-- egy kis tavolsag, hogy szebb legyyen -->
        <div class="container" style="margin-top:20px;"> &nbsp; </div>

        <?= $content ?>

    </div>
</div>







<footer class="footer">
    <div class="container">
        <p class="pull-left"> Szabika &copy; <?= date('Y') ?></p>

<!--        <p class="pull-right">--><?//= Yii::powered() ?><!--</p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $poe = Yii::$app->basePath.'/views/site/poe.php'; include $poe; ?>

<?php $this->endPage() ?>
