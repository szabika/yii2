<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */
/** @var $model \app\models\Kezdolap */

use yii\db\Query;



$this->title = 'My Yii Application';
?>
<!-- <?php echo \yii\bootstrap\Html::img('../images/bg_2.jpg', ['alt' => 'hatterkep']); ?> -->


<div class="site-index">

    <div class="jumbotron">
        <h1>Üdvözöllek!</h1>
        <p class="lead">Yii-powered Alkalmazás PTE állásinterjúra</p>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-8">
<!--            dinamikus tartalom CSAK kijelzesre, formok nem kellenek-->
                <h2><?php echo $oldal[0]->cim ?></h2>
                <?php echo $oldal[0]->tartalom ?>
            </div>



            <div class="col-lg-4">
                <h2><?php echo $oldal[1]->cim ?></h2>

                <?php echo $oldal[1]->tartalom ?>
            </div>
        </div>
    </div>
</div>
