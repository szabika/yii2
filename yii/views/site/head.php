<?php
/**
 * Created by PhpStorm.
 * User: szaboszabolcs
 * Date: 2021. 03. 20.
 * Time: 7:50
 */

use yii\helpers\Html;

// url es action
/*
 * <?= Yii::$app->request->url; ?>
 * <?= $act = Yii::$app->controller->action->id; ?>  //(index, about, contact, view, create, update, login)
 * */

$head_img = '';
$img_arr = array('bg_1.png','bg_2.jpg','bg_3.jpg','bg_4.jpg','bg_5.jpg','bg_6.jpg','bg_7.jpg');
$act_arr = array('index', 'about', 'contact','view', 'create', 'update', 'login');




$act = Yii::$app->controller->action->id;
$key = array_search($act, $act_arr);
$img_now = $img_arr[$key];

// $head_img = \Yii::$app->request->BaseUrl . "/images/" . $img_now;

if (!in_array($act, $act_arr)) { $head_img = \Yii::$app->request->BaseUrl . "/images/bg_1.png"; } else { $head_img = \Yii::$app->request->BaseUrl . "/images/" . $img_now; }

?>

<?php $mod = Yii::$app->basePath.'/views/site/modal.php'; include $mod; ?>

<section class="home-slider owl-carousel" style="width:100%;margin-top:30px">
    <div class="container"
         style="background-image:url('<?= $head_img ?>'); background-repeat: no-repeat; background-size: cover; height:400px; width:100%;">

        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
                <?= Html::img('@web/images/logo_pte.png', ['alt' => 'pic not found', 'style' => 'width:150px; height: 150px; margin-left:30px; margin-top: 20px;']) ?>
                <? //= $img_now ?><?//=$act ?>
        </div>
    </div>

</section>

<!-- Boxok csak a kezdolapon ********************************************************************************************************************************************** -->

<?php if (empty($act) || $act == 'index') : ?>


    <?php $feat = Yii::$app->basePath.'/views/site/features.php'; include $feat; ?>




<?php endif; ?>