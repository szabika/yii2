<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Kapcsolat';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Köszönöm a visszajelzésed.
        </div>

        <p>
            <strong>Szabó Szabolcs</strong><br/>

<!--            Note that if you turn on the Yii debugger, you should be able-->
<!--            to view the mail message on the mail panel of the debugger.-->

<!--            --><?php //if (Yii::$app->mailer->useFileTransport): ?>
<!--                Because the application is in development mode, the email is not sent but saved as-->
<!--                a file under <code>--><?//= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?><!--</code>.-->
<!--                Please configure the <code>useFileTransport</code> property of the <code>mail</code>-->
<!--                application component to be false to enable email sending.-->
<!--            --><?php //endif; ?>
        </p>

    <?php else: ?>

        <p>
            Ha szeretnél több információt kapni, várjuk visszajelzésed! Köszönjük!
        </p>

        <div class="row">
            <div class="col-lg-6">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label('Név') ?>

                    <?= $form->field($model, 'email')->label('E-mail cím') ?>

                    <?= $form->field($model, 'subject')->label('Tárgy') ?>

                    <?= $form->field($model, 'body')->textarea(['rows' => 6])->label('Üzenet') ?>

                    <?= $form->field($model, 'verifyCode')->label('Ellenőrző mező')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Küldés', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>


            <div class="col-lg-6">
                <?= Html::img('@web/images/map.png', ['alt' => 'gmap here', 'style' => 'width:100%; border:1px solid navy;margin-left:25px;']) ?>
            </div>
        </div>

    <?php endif; ?>
</div>
