<?php
/**
 * Created by PhpStorm.
 * User: szaboszabolcs
 * Date: 2021. 03. 22.
 * Time: 11:51
 */
use yii\helpers\Html;
?>

<div class="row no-gutters">
    <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-primary" style="background-color: #337ab7; height:180px; color:white; position: relative;">
        <div class="media block-6 d-block text-center">
            <div class="icon rounded-circle" style="margin-left:-90px;padding-bottom:35px;">
                <?= Html::img('@web/images/i1.png', ['alt' => 'pic not found', 'style' => 'width:100px; height: 100px;  margin-top:-50px; position: absolute; z-index:10;']) ?>
            </div>
            <div class="media-body p-2 mt-3">
                <h3 class="heading">Tanáraink</h3>
                <p>Oktatóink száma 135 fő, 82 fő PhD és DLA fokozattal rendelkezik, 29 százalék habilitált.</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-tertiary" style="background-color: #8dc059;  height:180px; color:white; position: relative;">
        <div class="media block-6 d-block text-center">
            <div class="icon rounded-circle" style="margin-left:-90px;padding-bottom:35px;">
                <?= Html::img('@web/images/i3.png', ['alt' => 'pic not found', 'style' => 'width:100px; height: 100px;  margin-top:-50px; position: absolute; z-index:10;']) ?>
            </div>
            <div class="media-body p-2 mt-3">
                <h3 class="heading">Kurzusaink</h3>
                <p>Nyolc alapképzésünk lefedi a műszaki, művészeti és az informatikai tudományterületet</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-fifth" style="background-color: #5d54c3;  height:180px; color:white; position: relative;">
        <div class="media block-6 d-block text-center">
            <div class="icon rounded-circle" style="margin-left:-90px;padding-bottom:35px;">
                <?= Html::img('@web/images/i2.png', ['alt' => 'pic not found', 'style' => 'width:100px; height: 100px;  margin-top:-50px; position: absolute; z-index:10;']) ?>
            </div>
            <div class="media-body p-2 mt-3">
                <h3 class="heading">Hallgatóink</h3>
                <p>Ezek vagyunk mink. Mert vagyunk. Hallgatók, tanulók, vizsgázók.</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-quarternary" style="background-color: #ee4743;  height:180px; color:white; position: relative;">
        <div class="media block-6 d-block text-center">
            <div class="icon rounded-circle" style="margin-left:-90px;padding-bottom:35px;">
                <?= Html::img('@web/images/i4.png', ['alt' => 'pic not found', 'style' => 'width:100px; height: 100px;  margin-top:-50px; position: absolute; z-index:10;']) ?>
            </div>
            <div class="media-body p-2 mt-3">
                <h3 class="heading">Garanciák</h3>
                <p>Diplomásoknak 13 szakmérnök-/szakember-képzést ajánlunk szakirányú továbbképzési céllal.</p>
            </div>
        </div>
    </div>
</div>
