<?php
/**
 * Created by PhpStorm.
 * User: szaboszabolcs
 * Date: 2021. 03. 18.
 * Time: 9:05
 */

/** @var $this \yii\web\View */
/** @var $model \app\models\User */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Regisztráció';
$this->params['breadcrumbs'][] = $this->title;

?>




<div class="site-register">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Kérlek, töltsed ki az alábbi mezőket a regisztrációhoz:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'register-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Felhasználónév')  ?>

        <?= $form->field($model, 'password')->passwordInput()->label('Jelszó')  ?>
        <?= $form->field($model, 'password_repeat')->passwordInput()->label('Jelszó újra')  ?>



    <p>Az általad megadott adatok kódoltan kerülnek letárolásra, és semmilyen esetben nem adjuk ki harmadik félnek.</p>
    <p style="padding-bottom:20px;">No, ide azért jó lenne egy ÁSZF, és egy GDPR friss tájékoztató, párhuzamosan a csokikezeléssel...</p>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>



</div>