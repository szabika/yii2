<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;


/**
 * This is the model class for table "cikkek".
 *
 * @property int $id
 * @property string $cim
 * @property string $link
 * @property string $meta_txt
 * @property string $meta_keys
 * @property string $text
 * @property int $created_by
 */
class Cikkek extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cikkek';
    }


    public function behaviors()
    {
        return [
            // timestamp is lehetne akár
            [
            'class' => SluggableBehavior::class,
            'slugAttribute' => 'link',
            'attribute' => 'cim'
            ]
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cim', 'meta_txt', 'meta_keys', 'text', 'created_by'], 'required'],   // 'link', automatára inkább
            [['text'], 'string'],
            [['created_by'], 'integer'],
            [['cim', 'link', 'meta_txt', 'meta_keys'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cim' => 'Cim',
            'link' => 'Link',
            'meta_txt' => 'Meta Txt',
            'meta_keys' => 'Meta Keys',
            'text' => 'Text',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveWuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }


    public function getEncodedBody()
    {
        return \yii\helpers\HtmlPurifier::process($this->text);
    }
}
