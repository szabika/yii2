<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kezdolap".
 *
 * @property int $id
 * @property string|null $cim
 * @property string|null $tartalom
 */
class Kezdolap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kezdolap';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['tartalom'], 'string'],
            [['cim'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cim' => 'Cim',
            'tartalom' => 'Tartalom',
        ];
    }
}
