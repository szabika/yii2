<?php
/**
 * Created by PhpStorm.
 * User: szaboszabolcs
 * Date: 2021. 03. 18.
 * Time: 9:00
 */

namespace app\models;

use yii\base\Model;
use yii\helpers\VarDumper;

/**
 * Class SignupForm
 *
 * @author Zura Sekhniashvili <zurasekhniashvili@gmail.com>
 * @package app\models
 */
class SignupForm extends Model
{
    public $username;
    public $password;
    public $password_repeat;

    public function rules()
    {
        return [
            [['username', 'password', 'password_repeat'], 'required'],
            ['username', 'string', 'min' => 4, 'max' => 16],
            [['password', 'password_repeat'], 'string', 'min' => 4, 'max' => 32],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password']
        ];
    }

    public function signup()
    {
        $user = new User();
        $user->username = $this->username;
        $user->password = \Yii::$app->security->generatePasswordHash($this->password);

        $user->auth_key = \Yii::$app->security->generateRandomString();
        $user->access_token = \Yii::$app->security->generateRandomString();


        if ($user->save()){
            return true;
        }

        \Yii::error("Felhasználó mentése nem sikerült: ".VarDumper::dumpAsString($user->errors));
        return false;
    }

}