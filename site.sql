-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Mar 22, 2021 at 06:09 AM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `pte_yii2_site`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(32) NOT NULL,
  `cim` varchar(255) DEFAULT NULL,
  `tartalom` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `cim`, `tartalom`) VALUES
(1, 'Mi a célja az ajánlólevélnek?', '    Az ajánlólevél vagy más néven referencialevél arra szolgál, hogy egy számodra még ismeretlen személy benyomást szerezzen rólad egy harmadik fél által. Ezáltal persze az önéletrajzodban feltüntetett adatokat is hitelesíti számára a harmadik személy.\r\n\r\n    Célszerű már akkor elkérnünk a munkaadónktól, amikor távozunk a munkahelyünkről, mert ilyenkor még friss az élmény mindkét félnek. Hónapok, de akár évek elteltével már valószínűleg nem ugyanazt az ajánlást fogjuk kapni, vagy már eleve nem akarnak ezzel foglalkozni.\r\n\r\n    <h2 style=\"font-size:19px;\">   Mikor lehet szükség rá? </h2>\r\n\r\n    Általában az álláspályázati anyaggal együtt, vagy az interjú során szoktak bekérni referencialevelet. Azonban fontos megjegyezni, hogy manapság már egyre ritkább ez a fajta kérés. Ennek az az oka, hogy ezeknél az ajánlóleveleknél nem lehet visszakérdezni/belekérdezni, illetve hogy általában az álláskereső már eleve olyan személytől kéri, akitől pozitív ajánlásra számít.\r\n\r\n    <h2 style=\"font-size:19px;\">   Mit kell, hogy tartalmazzon a levél? </h2>\r\n\r\n    Az ajánlólevél célja, hogy bemutasson téged a leendő munkáltatódnak az alábbiak szerint:\r\n    <ul>\r\n        <li>   mikor, hol és milyen pozícióban dolgoztatok együtt az ajánlóval </li>\r\n        <li> milyen feladatot láttál el, mi tartozott a munkakörödbe </li>\r\n        <li> milyen veled együtt dolgozni, mennyire vagy alkalmazkodó </li>\r\n        <li> milyen személyiség vagy, mennyire vagy megbízható és milyen a hozzáállásod </li>\r\n        <li> milyen a felkészültséged, szakmai tudásod </li>\r\n        <li> miért ért véget a munkaviszonyod </li>\r\n\r\n    </ul>\r\n    A levél végén a hitelesség érdekében fel kell tüntetni az ajánlást adó személy nevét és elérhetőségét, valamint így megadjuk a lehetőséget arra az esetre, ha további kérdéseket szeretnének feltenni neki.\r\n    </p>');

-- --------------------------------------------------------

--
-- Table structure for table `cikkek`
--

CREATE TABLE `cikkek` (
  `id` int(32) NOT NULL,
  `cim` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `meta_txt` varchar(255) NOT NULL,
  `meta_keys` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `created_by` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cikkek`
--

INSERT INTO `cikkek` (`id`, `cim`, `link`, `meta_txt`, `meta_keys`, `text`, `created_by`) VALUES
(1, 'Első bejegyzés', 'elso-bejegyzes', 'meta text ide', 'yii, beginner, ', '<p><strong>&Aacute;ll&aacute;shirdet&eacute;s</strong></p>\r\n\r\n<p>Az &bdquo;Adatok a szem&eacute;tdombr&oacute;l&rdquo; minisorozatban olyan eseteket mutatunk be, amelyekben kidobott vagy k&oacute;tyavety&eacute;re k&iacute;n&aacute;lt sz&aacute;m&iacute;t&oacute;g&eacute;pek adattartalm&aacute;t vizsg&aacute;ljuk. A sorozattal szeretn&eacute;nk felh&iacute;vni a c&eacute;gek &eacute;s mag&aacute;nszem&eacute;lyek figyelm&eacute;t arra, hogy a kiselejtezett eszk&ouml;z&ouml;k adattartalm&aacute;ra figyelni kell, az adatokat (lehetőleg) helyre&aacute;ll&iacute;thatatlanul el kell t&aacute;vol&iacute;tani, mielőtt az eszk&ouml;z selejtez&eacute;sre &eacute;s &eacute;rt&eacute;kes&iacute;t&eacute;sre ker&uuml;l.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"/yii/web/upload/files/pte.jpg\" style=\"height:354px; width:250px\" /></p>\r\n\r\n<p>Az &bdquo;Adatok a szem&eacute;tdombr&oacute;l&rdquo; minisorozatban olyan eseteket mutatunk be, amelyekben kidobott vagy k&oacute;tyavety&eacute;re k&iacute;n&aacute;lt sz&aacute;m&iacute;t&oacute;g&eacute;pek adattartalm&aacute;t vizsg&aacute;ljuk. A sorozattal szeretn&eacute;nk felh&iacute;vni a c&eacute;gek &eacute;s mag&aacute;nszem&eacute;lyek figyelm&eacute;t arra, hogy a kiselejtezett eszk&ouml;z&ouml;k adattartalm&aacute;ra figyelni kell, az adatokat (lehetőleg) helyre&aacute;ll&iacute;thatatlanul el kell t&aacute;vol&iacute;tani, mielőtt az eszk&ouml;z selejtez&eacute;sre &eacute;s &eacute;rt&eacute;kes&iacute;t&eacute;sre ker&uuml;l.&nbsp;</p>\r\n\r\n<p>A sorozatban azt is szeretn&eacute;nk bemutatni, hogy &aacute;ltal&aacute;nos forensic eszk&ouml;z&ouml;kkel hogyan lehet felt&aacute;rni egy merevlemez tartalm&aacute;t, hogyan lehet &bdquo;&eacute;rdekes &eacute;s hasznos&rdquo; dolgokat keresni az adatt&aacute;rol&oacute;n &eacute;s hogyan lehet &eacute;rt&eacute;kes adatokat előb&aacute;ny&aacute;szni egy szem&eacute;tdombra vetett vagy kiselejtezett eszk&ouml;zről.</p>\r\n\r\n<p><a id=\"more16437756\" name=\"more16437756\"></a></p>\r\n\r\n<p>&Eacute;s nem szabad elfeledkezni arr&oacute;l sem, hogy az adatok vissza&aacute;ll&iacute;that&oacute;s&aacute;g&aacute;nak probl&eacute;m&aacute;ja nem csak a kidobott adathordoz&oacute;kat &eacute;rinti. Ahogy egyre t&ouml;bb &eacute;s &uacute;jabb informatikai eszk&ouml;z, laptop, de főleg okostelefon jut el a felhaszn&aacute;l&oacute;khoz, &uacute;gy nő ezen eszk&ouml;z&ouml;k haszn&aacute;ltpiaci mennyis&eacute;ge is. &Eacute;s ezek az eszk&ouml;z&ouml;k ugyan&uacute;gy tartalmazhatnak olyan adatokat, amelyeket nem szeretn&eacute;nk megosztani a nagyvil&aacute;ggal, azonban a cikk&uuml;nkben bemutatott m&oacute;dszerek ezekre is &eacute;rv&eacute;nyesek.</p>\r\n', 3),
(2, 'Második bejegyzés auto CreBy', 'masodik-bejegyzes-auto-creby', 'asdf', 'asdf', '<p>N&eacute;ha c&eacute;lpontok &eacute;s &aacute;ldozatok vagyunk, n&eacute;ha pedig csak k&uuml;zdőt&eacute;r. A hazai kibert&eacute;rben most &eacute;pp a marokk&oacute;i hekkerek csaptak egyet oda Alg&eacute;ri&aacute;nak, illetve m&eacute;g pontosabban a budapesti alg&eacute;riai nagyk&ouml;vets&eacute;g weboldal&aacute;nak.</p>\r\n\r\n<p><a id=\"more16430888\" name=\"more16430888\"></a></p>\r\n\r\n<p>A Black Team nevű marokk&oacute;i hekkercsoport Allal al-Fassi &bdquo;Nagy Marokk&oacute;&rdquo; eszm&eacute;j&eacute;t k&ouml;vetve defacelte a budapesti nagyk&ouml;vets&eacute;g&nbsp;<a href=\"http://algerianembassy.hu/\" target=\"_blank\">weboldal&aacute;t</a>, ott adva hangot elk&eacute;pzel&eacute;s&uuml;knek, miszerint a Szahara Marokk&oacute;hoz tartozik.&nbsp;</p>\r\n\r\n<p>Marokk&oacute; &eacute;s Alg&eacute;ria t&ouml;rt&eacute;nelme nem &eacute;ppen a felt&eacute;tlen bizalom &eacute;s szeretet t&ouml;rt&eacute;nete, a csaknem 1600 km hossz&uacute; k&ouml;z&ouml;s hat&aacute;r, illetve a marokk&oacute;i nacionalizmus nemigen kedvez a bar&aacute;ts&aacute;guknak.&nbsp;</p>\r\n\r\n<p>Annakidej&eacute;n, az 1100-as &eacute;vekben a berber Almoravida birodalom tartotta kez&eacute;ben a Magreb t&eacute;rs&eacute;get, &iacute;gy bizonyos marokk&oacute;iak &uacute;gy gondolj&aacute;k, hogy Alg&eacute;ria tulajdonk&eacute;ppen Marokk&oacute; r&eacute;sze, bele&eacute;rtve (nyugat) Szahar&aacute;t, Tun&eacute;zi&aacute;t, tal&aacute;n m&eacute;g L&iacute;bi&aacute;t &eacute;s Maurit&aacute;ni&aacute;t is.</p>\r\n\r\n<p><img alt=\"allal_el_fassi_1949.png\" src=\"https://m.blog.hu/ki/kiber/image/allal_el_fassi_1949.png\" style=\"float:left; margin:0 2rem 1rem 0\" /></p>\r\n\r\n<p>1956-ban Allal al-Fassi, a marokk&oacute;i &iacute;r&oacute;, k&ouml;ltő, politikus &eacute;s tan&aacute;r sz&oacute;nokolt &bdquo;Nagy Marokk&oacute;r&oacute;l&rdquo;, illetve hogy vissza kell &aacute;ll&iacute;taniuk az Almoravida birodalmat &ndash; &eacute;s b&aacute;r ezt politikailag nemigen vett&eacute;k komolyan, ahogy az lenni szokott, a nem t&uacute;l iskol&aacute;zott &eacute;s nemigen paradicsomi &aacute;llapotok k&ouml;z&ouml;tt &eacute;lő sz&eacute;lesebb t&ouml;megek igencsak fellelkes&uuml;ltek az eszm&eacute;n. Od&aacute;ig fajult a dolog, hogy 1963-ban marokk&oacute;i csapatok t&aacute;madt&aacute;k meg Alg&eacute;ri&aacute;t, &eacute;s a szomsz&eacute;dok k&ouml;z&ouml;tti perpatvart nemzetk&ouml;zi szinten kellett elsim&iacute;tani, de l&aacute;that&oacute;an ez az&eacute;rt nem teljesen siker&uuml;lt. A nacionalista marokk&oacute;iak a mai napig Nagy Marokk&oacute;t vizion&aacute;ljak, &eacute;s a Balck Team hekkercsoport fennen hirdeti az ig&eacute;t a defacelt oldalakon.&nbsp;<br />\r\n1956-ban Allal al-Fassi, a marokk&oacute;i &iacute;r&oacute;, k&ouml;ltő, politikus &eacute;s tan&aacute;r sz&oacute;nokolt &bdquo;Nagy Marokk&oacute;r&oacute;l&rdquo;, illetve hogy vissza kell &aacute;ll&iacute;taniuk az Almoravida birodalmat &ndash; &eacute;s b&aacute;r ezt politikailag nemigen vett&eacute;k komolyan, ahogy az lenni szokott, a nem t&uacute;l iskol&aacute;zott &eacute;s nemigen paradicsomi &aacute;llapotok k&ouml;z&ouml;tt &eacute;lő sz&eacute;lesebb t&ouml;megek igencsak fellelkes&uuml;ltek az eszm&eacute;n. Od&aacute;ig fajult a dolog, hogy 1963-ban marokk&oacute;i csapatok t&aacute;madt&aacute;k meg Alg&eacute;ri&aacute;t, &eacute;s a szomsz&eacute;dok k&ouml;z&ouml;tti perpatvart nemzetk&ouml;zi szinten kellett elsim&iacute;tani, de l&aacute;that&oacute;an ez az&eacute;rt nem teljesen siker&uuml;lt. A nacionalista marokk&oacute;iak a mai napig Nagy Marokk&oacute;t vizion&aacute;ljak, &eacute;s a Balck Team hekkercsoport fennen hirdeti az ig&eacute;t a defacelt oldalakon.<br />\r\n1956-ban Allal al-Fassi, a marokk&oacute;i &iacute;r&oacute;, k&ouml;ltő, politikus &eacute;s tan&aacute;r sz&oacute;nokolt &bdquo;Nagy Marokk&oacute;r&oacute;l&rdquo;, illetve hogy vissza kell &aacute;ll&iacute;taniuk az Almoravida birodalmat &ndash; &eacute;s b&aacute;r ezt politikailag nemigen vett&eacute;k komolyan, ahogy az lenni szokott, a nem t&uacute;l iskol&aacute;zott &eacute;s nemigen paradicsomi &aacute;llapotok k&ouml;z&ouml;tt &eacute;lő sz&eacute;lesebb t&ouml;megek igencsak fellelkes&uuml;ltek az eszm&eacute;n. Od&aacute;ig fajult a dolog, hogy 1963-ban marokk&oacute;i csapatok t&aacute;madt&aacute;k meg Alg&eacute;ri&aacute;t, &eacute;s a szomsz&eacute;dok k&ouml;z&ouml;tti perpatvart nemzetk&ouml;zi szinten kellett elsim&iacute;tani, de l&aacute;that&oacute;an ez az&eacute;rt nem teljesen siker&uuml;lt. A nacionalista marokk&oacute;iak a mai napig Nagy Marokk&oacute;t vizion&aacute;ljak, &eacute;s a Balck Team hekkercsoport fennen hirdeti az ig&eacute;t a defacelt oldalakon.1956-ban Allal al-Fassi, a marokk&oacute;i &iacute;r&oacute;, k&ouml;ltő, politikus &eacute;s tan&aacute;r sz&oacute;nokolt &bdquo;Nagy Marokk&oacute;r&oacute;l&rdquo;, illetve hogy vissza kell &aacute;ll&iacute;taniuk az Almoravida birodalmat &ndash; &eacute;s b&aacute;r ezt politikailag nemigen vett&eacute;k komolyan, ahogy az lenni szokott, a nem t&uacute;l iskol&aacute;zott &eacute;s nemigen paradicsomi &aacute;llapotok k&ouml;z&ouml;tt &eacute;lő sz&eacute;lesebb t&ouml;megek igencsak fellelkes&uuml;ltek az eszm&eacute;n. Od&aacute;ig fajult a dolog, hogy 1963-ban marokk&oacute;i csapatok t&aacute;madt&aacute;k meg Alg&eacute;ri&aacute;t, &eacute;s a szomsz&eacute;dok k&ouml;z&ouml;tti perpatvart nemzetk&ouml;zi szinten kellett elsim&iacute;tani, de l&aacute;that&oacute;an ez az&eacute;rt nem teljesen siker&uuml;lt. A nacionalista marokk&oacute;iak a mai napig Nagy Marokk&oacute;t vizion&aacute;ljak, &eacute;s a Balck Team hekkercsoport fennen hirdeti az ig&eacute;t a defacelt oldalakon.</p>\r\n\r\n<p>1956-ban Allal al-Fassi, a marokk&oacute;i &iacute;r&oacute;, k&ouml;ltő, politikus &eacute;s tan&aacute;r sz&oacute;nokolt &bdquo;Nagy Marokk&oacute;r&oacute;l&rdquo;, illetve hogy vissza kell &aacute;ll&iacute;taniuk az Almoravida birodalmat &ndash; &eacute;s b&aacute;r ezt politikailag nemigen vett&eacute;k komolyan, ahogy az lenni szokott, a nem t&uacute;l iskol&aacute;zott &eacute;s nemigen paradicsomi &aacute;llapotok k&ouml;z&ouml;tt &eacute;lő sz&eacute;lesebb t&ouml;megek igencsak fellelkes&uuml;ltek az eszm&eacute;n. Od&aacute;ig fajult a dolog, hogy 1963-ban marokk&oacute;i csapatok t&aacute;madt&aacute;k meg Alg&eacute;ri&aacute;t, &eacute;s a szomsz&eacute;dok k&ouml;z&ouml;tti perpatvart nemzetk&ouml;zi szinten kellett elsim&iacute;tani, de l&aacute;that&oacute;an ez az&eacute;rt nem teljesen siker&uuml;lt. A nacionalista marokk&oacute;iak a mai napig Nagy Marokk&oacute;t vizion&aacute;ljak, &eacute;s a Balck Team hekkercsoport fennen hirdeti az ig&eacute;t a defacelt oldalakon.</p>\r\n', 5),
(3, 'Meghekkelték Palvin Barbarát', 'meghekkeltek-palvin-barbarat', 'asdf asdf asdf ', 'dsf asdf asd', '<p>Az oroszokt&oacute;l &eacute;s a k&iacute;naiakt&oacute;l retteg&uuml;nk, k&ouml;zben meg olyan oldalr&oacute;l j&ouml;n a pofon, amerről egy&aacute;ltal&aacute;n nem sz&aacute;m&iacute;tottunk: offenz&iacute;v&aacute;t ind&iacute;tottak Magyarorsz&aacute;g ellen a t&ouml;r&ouml;k&ouml;k. J&oacute;l mondta F&uuml;lig Jimmy: Nem lehet minden pofon mell&eacute; forgalmi rendőrt &aacute;ll&iacute;tani.</p>\r\n\r\n<p><a id=\"more16393662\" name=\"more16393662\"></a>Igen akt&iacute;vak a t&ouml;r&ouml;k&ouml;k mostan&aacute;ban a hazai kibert&eacute;rben. M&aacute;r meghackelt&eacute;k a&nbsp;<a href=\"https://kiber.blog.hu/2021/01/17/meg_nem_halal_a_burger_king_de_a_torokok_mar_a_burgerben_vannak\" target=\"_blank\">Burger King Magyarorsz&aacute;g Instagram</a>&nbsp;fi&oacute;kj&aacute;t, &eacute;s defacelt&eacute;k Palvin Barbara oldal&aacute;t, a palvinbarbara.hu-t is.</p>\r\n\r\n<p><strong>H&aacute;t mi van itt??<br />\r\n<br />\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/yii/web/upload/files/e303b865d4d1b15df0811ae37d1a18d6_d49afe6e3b4eb7cadfe308837303ec67.jpg\" style=\"\"/></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Digit&aacute;lis Moh&aacute;cs, az van! Krasznay doktor v&iacute;zi&oacute;ja val&oacute;s&aacute;gg&aacute; v&aacute;lik?!!&nbsp;</p>\r\n\r\n<p>Valamit most m&aacute;r tenni kell a t&ouml;r&ouml;k&ouml;kkel, &eacute;n mondom!</p>\r\n\r\n<p>Az nem j&aacute;rja, hogy a r&eacute;gi idők szerint elrabolj&aacute;k le&aacute;nyainkat &eacute;s asszonyainkat! Barbit meg k&uuml;l&ouml;nben sem adjuk! Az egyetlen t&eacute;nyleges &eacute;s val&oacute;s hungarikumot t&aacute;madt&aacute;k meg, ez v&eacute;rl&aacute;z&iacute;t&oacute; &eacute;s tűrhetetlen.</p>\r\n\r\n<p>Nom&aacute;d kibermigr&aacute;ncs-hord&aacute;k lepik el sz&eacute;ps&eacute;ges magyari digit&aacute;lis mezeinket, gyilkos janics&aacute;rok kaszabolj&aacute;k halomra szervereinket.</p>\r\n\r\n<p>A hat&aacute;ron ott van a Ker&iacute;t&eacute;s. Az alatt csak vakondok, pockok vagy k&uuml;l&ouml;nlegesen kistermetű, &nbsp;bergmandli migr&aacute;ncsok juthatnak &aacute;t, de k&ouml;zben meg a digit&aacute;lis mezeinken szabadon gar&aacute;zd&aacute;lkodnak a t&ouml;r&ouml;k hekkerek. Hogy &aacute;llna keresztbe benn&uuml;k a pil&aacute;fos kan&aacute;l!</p>\r\n\r\n<p>A deface-t elk&ouml;vető T&uuml;rkDef csoport ezzel kih&uacute;zta a gyuf&aacute;t. Megszents&eacute;gtelen&iacute;tett&eacute;k Barbi oldal&aacute;t, &eacute;s ki&iacute;rt&aacute;k r&aacute;, hogy:</p>\r\n\r\n<p>&nbsp;Azt javaslom, hogy bossz&uacute;b&oacute;l ne egy&uuml;nk d&uuml;r&uuml;m&ouml;t meg kebabot am&iacute;g bocs&aacute;natot nem k&eacute;rnek!</p>\r\n\r\n<p>A T&uuml;rkDef csoporttal kor&aacute;bban a SecNews k&eacute;sz&iacute;tett interj&uacute;t. A csoport oldala deface szempontb&oacute;l el&eacute;gg&eacute; impoz&aacute;ns, a g&ouml;r&ouml;g Toyota, a Dechatlon, a National Geographic, a Linkin Park, Robbie Williams, a CNN &eacute;s a Mitsubishi is a skalpjaik k&ouml;z&eacute; tartoznak.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/yii/web/upload/files/narnara.png\" style=\"width:620px\"/></p>\r\n\r\n<p>A palvinbarbara.hu-t m&eacute;g janu&aacute;r 14-&eacute;n t&aacute;madt&aacute;k meg, &eacute;rdekes, hogy az&oacute;ta az oldal eredeti tartalma m&eacute;g nem ker&uuml;lt vissza&aacute;ll&iacute;t&aacute;sra.&nbsp;</p>\r\n\r\n<p><strong>UPDATE</strong></p>\r\n\r\n<p><strong>2021.01.18</strong></p>\r\n\r\n<p>Az oldal tartalma t&ouml;r&ouml;lve lett. Egy&eacute;bk&eacute;nt elk&eacute;pzelhető, hogy egy mag&aacute;ra hagyott, nem gondozott oldal volt (benfentes KZ4 koll&eacute;ga szerint - ki oly j&aacute;ratos a celebvil&aacute;gban - Barbi m&aacute;r nem haszn&aacute;lta az oldalt). De akkor is. Nem hazafi aki kebabot &eacute;s d&uuml;r&uuml;m&ouml;t eszik am&iacute;g bocs&aacute;natot nem k&eacute;rnek a gyal&aacute;zat&eacute;rt!</p>\r\n', 3);

-- --------------------------------------------------------

--
-- Table structure for table `kezdolap`
--

CREATE TABLE `kezdolap` (
  `id` int(11) NOT NULL,
  `cim` varchar(255) DEFAULT NULL,
  `tartalom` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kezdolap`
--

INSERT INTO `kezdolap` (`id`, `cim`, `tartalom`) VALUES
(1, 'Kezdőlap', '<p>Általános leírás, üdvözlő szöveg, blah, blah...</p>\r\n\r\n                <p>Lórum ipse már nem kálkás, hanem fárult. a cseszkes bolatlan és sértes vacsák, mint a hami\r\n                    - és sokszor loncos - pityó. Kezességgel is hábol, troszban a sülsős akkor bölt bakt pongásának\r\n                    handájává arakították. 49 tajtagias, 25 tajtagias egykéje és 19 tajtagias zafasága van. - a batus,\r\n                    a foszt és a sítő mellett hogyan páromjaz kedése a taglára? - szigos elenség vannak,\r\n                    így több kedés páromjaz a dalan iségre. - Abban a kedésben siktekenyész a solygós pacskáit kodta.\r\n                    Erre a csozápra akkor a sarnos fűzésök voltak vérgesek, siktekenyész mégis a nyuga szerációkat arakította.\r\n                    Deketre később golózott a csorom, a vasztában és a szelenben is eskedtek a csorom farzásai.</p>\r\n                <p>Senki sem ápítgatott vele egyet, inkább könyelep a füröntőbe hérogt, ahelyett, hogy lizált volna rajta.\r\n                    A varzást kell vistás - persze, ha van. Lehet róla ikést élcelőznie, de elég pokonyak ahhoz,\r\n                    hogy azt kedőnek lehessen intoroznia. 5 sontás alatt bele lehet verentnie valakinek az ötrenzésébe,\r\n                    a tordjába, az aggságaiba, a leperányába, bármibe, de ez egyáltalán nem lemző a lományhoz és az üstörnyő túdásához.\r\n                    És tényleg lehet szegő ikést, más romorzsát sedintnie érlésen keresztül...\r\n                    Az egyáltalán nem azt szabanítja, hogy nem is fabolnak vedével, hanem csak azt,\r\n                    hogy nem csavaszkáznak vedével bávány cándást. Mit pajhárog el a remény a henségről?</p>\r\n                <p>&nbsp</p>\r\n                <p>&nbsp</p> <!-- keskeny méreten csúnyán közel van a 4-es blokk -->'),
(2, 'Belépés', '<p>Belépés a menü Login gombja alatt!</p>\n<p>A teszteléshez létrehozott felhasználónév / jelszó páros a következő: <br/>\n<span style=\"color:#9c2b14\">KivancsiSzemek / admin1234 </span><br/>\n<pre style=\"display: block; \">(az IT-biztonság maximális támogatása mellett...)</pre>\n</p>');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(32) NOT NULL,
  `username` varchar(55) NOT NULL,
  `password` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `access_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `auth_key`, `access_token`) VALUES
(3, 'KivancsiSzemek', '$2y$13$O32S6zTaviucUtCzns/HdOXz3yHB1Qg6KuGSZztdqt5HAAYAxswHS', 'Z61SgIJd9zUE_ZKU7Bd0nOcqsd86-uHa', 'Ezn0llGgOmx6HpYi5xl15xupbyiibaxO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cikkek`
--
ALTER TABLE `cikkek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kezdolap`
--
ALTER TABLE `kezdolap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cikkek`
--
ALTER TABLE `cikkek`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
