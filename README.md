**Telepítési útmutató**

Futtatási követelmény: xamp, lamp, mamp, wamp, vagy internetes hostingtól bérelt webkiszolgáló.


1) 

Töltsd le a yii repo tartalmát csomagban a gépre, majd csomagold ki a webszerver mappájába.

Ha a Git telepítve van: 

Klónozd le a webroot-ba a projektet


Néhány példa a webszerver mappájához:

Linux:
`$ cd /home/user/my_project`
`$ cd /var/www/html`

MacOS:
`$ cd /Users/user/Sites`
`$ cd /Applications/MAMP/htdocs)`


Windows:
`$ cd C:/Users/user/my_project`
`$ cd C:/xampp/htdocs/`




`$ git clone https://gitlab.com/szabika/yii2.git`

------------------

2) Minta adatbázis telepítése:
`site.sql` file tartalmazza a teszteléshez szükséges adattábblákat és adatokat.

3) Adatbázis kapcsolat beállítása a

`yii/config/db.php` 

fájlban

```
    'dsn' => 'mysql:host=HOST;port=PORT;dbname=DBName', 
    'username' => 'MySQL USER',
    'password' => 'MySQL PASSW',
    'charset' => 'utf8',
```




4) megnyitás böngészőben a teszteléshez

Helyi szerveren:

`localhost:PORT/yii/web `

Távoli (online) szerveren:

`http://DOMAIN/yii/web`



A webalkalmazás alapértelmezett indulási mappája a yii/web könyvtár.

Online tesztelésnél szükség lehet a yii/web könyvtárban lévő
`.htaccess` (rejtett) file módosítására, az induló könyvtárral 
való kiegészítésére:

`RewriteBase /yii/web`




